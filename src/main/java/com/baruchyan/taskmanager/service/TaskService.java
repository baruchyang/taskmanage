package com.baruchyan.taskmanager.service;

import com.baruchyan.taskmanager.entity.Task;
import com.baruchyan.taskmanager.repository.TaskRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TaskService {

    @Autowired
    private TaskRepository taskRepository;

    public List<Task> getAllTasks(){
        return taskRepository.findAll(Sort.by(
                Sort.Order.asc("date"),
                Sort.Order.desc("priorityId")
        ));
    }

    public Task save(Task task){
        return taskRepository.save(task);
    }

    public void delete(int id){
        taskRepository.deleteById(id);
    }
}
