package com.baruchyan.taskmanager.controller;

import com.baruchyan.taskmanager.entity.Task;
import com.baruchyan.taskmanager.service.TaskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
public class TaskController {

    @Autowired
    private TaskService taskService;

    @GetMapping("/")
    public String getAllTasks(Model model){
        List<Task> tasks = taskService.getAllTasks();

        model.addAttribute("taskList", tasks);
        model.addAttribute("taskSize", tasks.size());

        return "Index";
    }

    @RequestMapping("/delete/{id}")
    public String delete(@PathVariable int id){
        taskService.delete(id);
        return "redirect:/";
    }

    @PostMapping("/add")
    public String add(@ModelAttribute Task task){
        taskService.save(task);
        return "redirect:/";
    }
}
